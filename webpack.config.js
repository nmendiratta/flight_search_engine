/*
 *     ./webpack.config.js
 *     */
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: './client/index.html',
  filename: 'index.html',
  inject: 'body'
});

module.exports = {
  entry: './client/index.js',
  output: {
    path: path.resolve('dist'),
    filename: 'index_bundle.js'
  },
  module: {
    loaders: [ //to transpile js from .js and .jsx files exlluding node modules
      {test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/},
      {test: /\.jsx$/, loader: 'babel-loader', exclude: /node_modules/}
    ]
  },
  // to inject javascript in html body https://scotch.io/tutorials/setup-a-react-environment-using-webpack-and-babel#toc-html-webpack-plugin
  plugins: [HtmlWebpackPluginConfig]
};
